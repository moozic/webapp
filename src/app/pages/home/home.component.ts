import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api/api.service';
import { Category } from 'src/app/models/category.model';
import { Song } from 'src/app/models/song.model';
import { environment } from 'src/environments/environment';
import { DomSanitizer } from '@angular/platform-browser';
import { Songs } from 'src/app/models/songs.model';
import { Hashtag } from 'src/app/models/hashtag.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  env = environment;
  
  categories: Category[];
  songs: Songs;
  song: Song;
  hashtags: Hashtag[];

  playStatus = false;
  audio:any=null;

  currentSongIndex = 0;
  currentPage = 1;
  currentCategory = 0;
  

  constructor(
    private api: ApiService,
    private sanitizer: DomSanitizer
  ) { }

  ngOnInit(): void {
    this.getAllCategories();
    this.getAllSongs();
    this.getAllHashtags();
  }

  getAllCategories() {
    this.api.get("categories").subscribe((data: Category[]) => {
      console.log("getAllCategories",data);
      this.categories = data;
    })
  }

  getAllSongs() {
    this.api.get("songs").subscribe((data: Songs) => {
      console.log("getAllSongs",data);
      this.songs = data;
    })
  }

  getAllHashtags() {
    this.api.get("hashtags").subscribe((data: Hashtag[]) => {
      console.log("getAllHashtags",data);
      this.hashtags = data;
    })
  }

  getAllHashtagsByCategory() {
    this.api.get("hashtags/by/category/" + this.currentCategory).subscribe((data: Hashtag[]) => {
      console.log("getAllHashtags",data);
      this.hashtags = data;
    })
  }

  getSongsByCategory() {
    this.api.get("songs/category/"+ this.currentCategory + '?page=' + this.currentPage).subscribe((data: Songs) => {
      console.log("getSongsByCategory",data);
      this.songs = data;
      this.song = this.songs.data[this.currentSongIndex];
      this.audio=new Audio(this.env.uploadPath + 'mp3/' + this.song.mp3);
      this.playSong();
    })
    this.getAllHashtagsByCategory();
  }

  showSong(index) {
    this.currentSongIndex = index;
    if(this.audio) this.pauseSong();
    this.song = this.songs.data[index];
    this.audio=new Audio(this.env.uploadPath + 'mp3/' + this.song.mp3);
  }
  
  getSafeStyle(url: string) {
    return this.sanitizer.bypassSecurityTrustStyle('url(' + this.env.uploadPath + 'covers/' + url + ')');
  }

  playSong() {
    this.playStatus = true;
    this.audio.play();
  }

  pauseSong() {
    this.playStatus = false;
    this.audio.pause();
  }

  // nextSong() {
  //   this.pauseSong();
  //   this.currentSongIndex++;
  //   this.song = this.songs.data[this.currentSongIndex];
  //   this.audio=new Audio(this.env.uploadPath + 'mp3/' + this.song.mp3);
  //   this.playSong();
  // }

  nextSong() {
    this.pauseSong();
    if((this.currentSongIndex+1) == this.songs.data.length) {
      this.currentSongIndex = 0;
      this.currentPage++;
      this.getSongsByCategory();
      
    }else {
      this.currentSongIndex++;
    
      this.song = this.songs.data[this.currentSongIndex];
      this.audio=new Audio(this.env.uploadPath + 'mp3/' + this.song.mp3);
      this.playSong();
    }

  }

  prevSong() {
    this.pauseSong();
    this.currentSongIndex--;
    this.song = this.songs.data[this.currentSongIndex];
    this.audio=new Audio(this.env.uploadPath + 'mp3/' + this.song.mp3);
    this.playSong();
  }

  changePage(page){
    console.log("page",page);
    this.currentPage = page;
    this.getSongsByCategory();
  }
}
