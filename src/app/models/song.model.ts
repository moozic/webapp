export interface Song {
    artist_id: number,
    category_id: number,
    created_at: string,
    desc: string,
    id: number,
    image: string,
    mp3: string,
    title: string,
    updated_at: string,
    artist: any
}
