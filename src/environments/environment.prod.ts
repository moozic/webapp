export const environment = {
  production: true,
  apiUrl: 'http://api.moozic.mt/api/',
  uploadPath: 'http://cdn.moozic.mt/upload/'
};
